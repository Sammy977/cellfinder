import os
import xlrd
import openpyxl
from openpyxl import load_workbook

cell_type = input("What's the type of your cellule?")
cell_number = input("What's the reference of your cellule?")
user_dir = os.path.expanduser("~")
directory = (user_dir + "\\VOCSens\\VOCSens - Documents\\EnviCam\\Sensing elements\\Alphasense_Calibration\\" + str(
    cell_type))
sortie = ''
dictionary = {}

# Get a list of all files in the directory
files = os.listdir(directory)


# Create a function for the dictionary in output
def create_dictionary(row, cell_number, cell_type, file):
    if str(row[1].value) == cell_number:
        if cell_type == 'OX':
            dictionary.update({
                'Serial No': row[1].value,
                'Zero Current': row[2].value,
                'O3 Sensisivity': row[4].value,
                'NO2 Sensitivity': row[5].value,
                'Nom de fichier': file
            })
        else:
            dictionary.update({
                'Serial No': row[1].value,
                'Zero Current': row[2].value,
                'Sensisivity': row[4].value,
                'Nom de fichier': file
            })
    return dictionary


# Filter the list to only include Excel files
excel_files = [f for f in files if f.endswith(('.xlsx', '.xls'))]
for file in excel_files:
    if file.endswith('.xls'):
        file_path = os.path.join(directory, file)
        workbook = xlrd.open_workbook(file_path)
        sheet = workbook.sheet_by_index(0)

        for row in sheet.get_rows():
            dict = create_dictionary(row, cell_number, cell_type, file)


    elif file.endswith('.xlsx'):
        file_path = os.path.join(directory, file)
        workbook = openpyxl.load_workbook(file_path)
        sheet = workbook.active

        # Search for the cell_number in the first column of the sheet
        for row in sheet.iter_rows(min_row=0, max_row=100):
            dict = create_dictionary(row, cell_number, cell_type, file)

if dict != {}:
    print(dict)
else:
    print('There\'s no cells with that serial number')
